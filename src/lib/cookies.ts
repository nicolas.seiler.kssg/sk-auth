export const idTokenCookieName = 'svelteauth_id_token';
export const refreshTokenCookieName = 'svelteauth_refresh_token';
export const expiresAtCookieName = 'svelteauth_expires_at';
export const providerCookieName = 'svelteauth_provider';
